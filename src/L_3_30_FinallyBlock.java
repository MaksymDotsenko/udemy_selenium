public class L_3_30_FinallyBlock {
    public static void main(String[] args) {

            final int x=100;
            //x=200; // gives an error --> final (class, method, variable) could not be changed
        try{

            // DB Connection
            // executing some queries
            // validation the data an comparing from websites
            // Closing connection

            int i[]=new int[4];
            i[5] =100;
            System.out.println("Closing DB Connection in try block");
        }catch (Exception e){
            System.out.println("Error occurred");
        }finally {
            System.out.println("Closing the DB Connection");
        }
    }
}
