public class L_3_29_ArrayException {
    public static void main(String[] args) {
        System.out.println("Beginning");
        try {


            int i[] = new int[4];

            i[5] = 100;
        }catch (Throwable t){
            System.out.println("Error occurred");
            t.printStackTrace();
            // generate screenshot possible
            // send email with attached screenshot and print error message
            // in mail subject
        }
        System.out.println("Ending");
    }
}
