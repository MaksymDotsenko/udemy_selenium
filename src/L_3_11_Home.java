public class L_3_11_Home {

    //instance / local // class variables

    /*
    *
    * instance --- declares class body
    *
    * local variables --- declared inside method body
     */

    int i = 123; // instance variable -- Global variable
    // this variable belongs to the instance of this class

    public void go(){

        //i = 456;
        int j = 234; // local variable
        //System.out.println(i);
    }

    public void show(){

        //i=7438; // not possible
        //j=384526;
    }


    public static void main(String[] args) {

       // System.out.println(i);

    }
}
