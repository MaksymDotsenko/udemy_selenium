package L_3_33_Inharitance;

//Dog & BullDog & Animal --> Multilevel Inheritance (BullDog is SubClass of Dog; Dog is a SubClass of the Animal (and it is SuperClass))

public class BullDog extends Dog {
    public static void main(String[] args) {

        BullDog obj = new BullDog();
        obj.sound();
    }
}
