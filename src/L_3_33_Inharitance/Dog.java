package L_3_33_Inharitance;

//Dog and Animal --> Single level Inheritance (Dog is SubClass/ChildClass; Animal is SuperClass)
//Dog & Cat & Animal --> Hierarchical level Inheritance (Dog and Cat is SubClass/ChildClass; Animal is SuperClass)
//Dog & BullDog & Animal --> Multilevel Inheritance (BullDog is SubClass of Dog; Dog is a SubClass of the Animal (and it is SuperClass))
public class Dog extends Animal{

    public static void main(String[] args) {

        Dog obj = new Dog();
        obj.sound();
    }

    public void sound(){

        System.out.println("whow whow --> Dog");
    }
}
