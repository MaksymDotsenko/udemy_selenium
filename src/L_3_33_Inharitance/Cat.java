package L_3_33_Inharitance;

//Dog & Cat & Animal --> Hierarchical level Inheritance (Dug and Cat is SubClass/ChildClass; Animal is SuperClass)

public class Cat extends Animal {

    public static void main(String[] args) {

        Cat obj = new Cat();
        obj.sound();
    }
}
