package L_3_33_Inharitance;

public class InheritanceExemple {
    public static void main(String[] args) {

        /*
        *
        * Type of Inheritance:
        *
        * Single
        * Multiply
        * Multilevel
        * Hierarchical
        *
        *
        * Class --> class / interface--> interface-->extends
        * Class --> interface --> implements
        *
        * Class Level:
        *
        * Single inheritance
        * MultiLevel inheritance
        * Hierarchical inheritance
        *
        * Interface Level:
        *
        * Single inheritance
        * MultiLevel inheritance
        * Hierarchical inheritance
        * Multiply inheritance //ChildClass extends two SuperClasses --> Not possible in java only with interfaces!
        *
         */
    }
}
