package L_3_40_Writing_TXT_CSV;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WritingFiles {

    public static void main(String[] args) throws IOException {

        //Stream connectivity
        //File f = new File(System.getProperty("user.dir")+"L_3_40_Writing_TXT_CSV/fileWriting/newfile.txt");
        File f = new File("/home/maksymd/IdeaProjects/Java_Lecture2/src/L_3_40_Writing_TXT_CSV/fileWriting/newfile.txt");
        System.out.println(System.getProperty("user.dir"));

        FileWriter fw = new FileWriter(f, false);
        BufferedWriter bf = new BufferedWriter(fw);

        //Writing inside the file
        for (int i=0; i<5; i++){
            for (int j=0; j<3; j++){
                int num = (int) (Math.random()*100);
                bf.write(num+" \t");
            }
            bf.newLine();
        }

        bf.newLine();
        bf.write("Therd Line");
        bf.newLine();
        bf.write("Ukraine");
        bf.newLine();
        bf.write("Way2Automation");
        bf.newLine();
        bf.write("Roman");

        //Closing Stream
        bf.close();
        System.out.println("File created!!!");
    }
}
