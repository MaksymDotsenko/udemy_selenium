package L_3_34_Polymorpism;

import java.security.PublicKey;

public class OverLoadingExample {

    /*int a;
    int b;
    int c;*/

    public static void main(String a ) {
        //we also can Overloading main method
    }

    public static void main(int a) {
        //we also can Overloading main method
    }
    public static void main(String[] args) {
        OverLoadingExample obj = new OverLoadingExample();
        //obj.add(a, b);
        //obj.add(a, b, c);
        obj.add(12 ,23); //int (cntr+mouse)
        obj.add(1,2); // byte (cntr+mouse)
        obj.add(1,2,3);
        obj.doLogin("Max", "1234");
        obj.doLogin(1231231412, "1234");
        byte b1=10;
        byte b2=20;
        obj.add(b1,b2);
        //new example
        //obj.add2(1,2); //Would be runtime Error/Compile error (ambiguous/двусмысленный)

        //new example2 ---> varargs
        obj.addition(10,20,30,40,50);

    }

    public void add(int a, int b){

    }
    public void add(byte a, byte b){

    }
    //new example
    public void add2(double a, int b){

    }
    public void add2(int a, double b){
    //only in this way --> if (double a, double b) & (double a, int b) would work
    }

    /*public int add(int a, int b){ //void --> int

    }*/
    //by changing the return type the overloading will not work!

    /*public int add(int a, int new){ //int a --> int newe

    }*/
    //by changing variable the overloading will not work!

    public void add(int a, int b, int c){

    }
    public void doLogin(String username, String password){

    }
    public void doLogin(int mobilenumber, String password){

    }

    //new example2 ---> varargs
    public void addition(int... a){
    //how to add this all values
        int[] var = a;
        System.out.println(var); // problem with sum????????
    }

}
