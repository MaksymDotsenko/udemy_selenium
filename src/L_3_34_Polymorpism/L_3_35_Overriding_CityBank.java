package L_3_34_Polymorpism;

public class L_3_35_Overriding_CityBank extends L_3_35_Overriding_RBIbank {

    public static void main(String[] args) {

        L_3_35_Overriding_CityBank obj = new L_3_35_Overriding_CityBank();
        System.out.println(obj.getHomeLoanROI());

        L_3_35_Overriding_CityBank obj2 = new L_3_35_Overriding_CityBank();
        L_3_35_Overriding_RBIbank obj3 = new L_3_35_Overriding_CityBank(); // possible with Polimorphism and Inharitance(Extends)
        //L_3_35_Overriding_CityBank obj4 = new L_3_35_Overriding_RBIbank(); //Child class can't be referenced to parent class
    }

    @Override
    public double getHomeLoanROI() {
        //return super.getHomeLoanROI(); // by default
        return  10.5;
    }

    public Integer show() {//Class Number --> class Integer //Covariant - the return types are className and there is parent - child relations between those classes
        return 10;
    }

}
