package L_3_34_Polymorpism.L_3_36_Overriding;

public class ChromeDriver extends WebDriver {

    public void clickOnChrome(){

        System.out.println("Clicking in Chrome");

    }

    public void sendKeysOnChrome(){

        System.out.println("Typing in Chrome");

    }
    //OR

    public void click(){
        System.out.println("Clicking in Chrome");
    }
    public void sendKeys(){
        System.out.println("Typing in Chrome");
    }

}
