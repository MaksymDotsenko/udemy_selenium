package L_3_34_Polymorpism.L_3_36_Overriding;

public class WebDriver {

    public void click(){

        System.out.println("Performing click - WebDriver()");
    }

    public void sendKeys(){

        System.out.println("Typing in an Element - WebDriver()");
    }

    public void getTitel(){

        System.out.println("Getting the Title - WebDriver()");
    }
}
