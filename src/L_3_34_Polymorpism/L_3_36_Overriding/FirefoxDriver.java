package L_3_34_Polymorpism.L_3_36_Overriding;

public class FirefoxDriver extends WebDriver{

    public void clickOnFF(){
        System.out.println("Clicking in FF");


    }

    public void sendKeysOnFF(){
        System.out.println("Typing in FF");

    }
    //OR

    public void click(){
        System.out.println("Clicking in FF");
    }
    public void sendKeys(){
        System.out.println("Typing in FF");
    }

}
