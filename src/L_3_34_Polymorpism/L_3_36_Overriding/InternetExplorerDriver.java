package L_3_34_Polymorpism.L_3_36_Overriding;

public class InternetExplorerDriver extends WebDriver {

    public void clickOnIE() {
        System.out.println("Clicking in IE");

    }

    public void sendKeysOnIE() {
        System.out.println("Typing in IE");

    }
    //OR

    public void click() {
        System.out.println("Clicking in IE");
    }

    public void sendKeys() {
        System.out.println("Typing in IE");
    }
}
