package L_3_34_Polymorpism.L_3_36_Overriding;

// Q: Can we change the access modifier when overriding?
// YES: It could be but we cannot reduce the visibility!


//Q: Can we override static methods?
// NO --> example - Method hiding
public class Parent {
    //Example
    public static void stat(){
        System.out.println("stat - Parent()");

    }

    public void show(){

    }
    public void add(){

    }
}
