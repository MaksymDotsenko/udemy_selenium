package L_3_34_Polymorpism.L_3_36_Overriding;

public class TestCase1 extends Base{
    //OR with Base class
    String browserName="FireFox";


    public static void main(String[] args) {

        FirefoxDriver driver = new FirefoxDriver();
        driver.click();
        driver.sendKeys();
        driver.getTitel();
        //for specific Browser
        driver.clickOnFF();
        driver.sendKeysOnFF();

        InternetExplorerDriver driver2 = new InternetExplorerDriver();
        driver2.click();
        driver2.sendKeys();
        driver2.getTitel();
        //for specific Browser
        driver2.clickOnIE();
        driver2.sendKeysOnIE();

        ChromeDriver driver3 = new ChromeDriver();
        driver3.click();
        driver3.sendKeys();
        driver3.getTitel();
        //for specific Browser
        driver3.clickOnChrome();
        driver3.sendKeysOnChrome();

        //OR with Base class
        TestCase1 tc =new TestCase1();
        tc.initBrowser();



    } //OR with Base class
    public void initBrowser(){
        WebDriver driver = getBrowserInstance(browserName);
        driver.click();
        driver.sendKeys();
    }
}
