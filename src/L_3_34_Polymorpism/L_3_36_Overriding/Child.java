package L_3_34_Polymorpism.L_3_36_Overriding;

public class Child extends Parent{
    public void show(){

    }
    public void display(){

    }
    //example Q:

    public static void stat(){
        System.out.println("stat - Child()");
    }

    public static void main(String[] args) {
        Parent c = new Child();
        c.show();
        c.add();
        //c.display(); // only in child class

        ((Child) c).stat();
        c.stat();
    }
}
