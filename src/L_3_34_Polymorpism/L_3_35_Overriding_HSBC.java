package L_3_34_Polymorpism;

public class L_3_35_Overriding_HSBC extends L_3_35_Overriding_RBIbank{

    public static void main(String[] args) {

        L_3_35_Overriding_HSBC obj = new L_3_35_Overriding_HSBC();
        System.out.println(obj.getHomeLoanROI());
    }

    @Override
    public double getHomeLoanROI() {
        //return super.getHomeLoanROI(); // by default
        return  10.25;
        // return int --->
        // yes: we can not change return type while performing overriding
        //  no: in case the return type are className(covariant)

        //Covariant - the return types are className and there is parent - child relations between those classes
    }

    public L_3_35_Overriding_AMEX getObject() {

        L_3_35_Overriding_AMEX obj = new L_3_35_Overriding_AMEX();
        return obj;
    }

}
