package L_3_34_Polymorpism;

//Overriding / dynamic binding / late binding / runtime polymorphism
//different classes (with inheritance applied) we have methods with same signature

public class L_3_35_Overriding_RBIbank {

    public double getHomeLoanROI() {
        return 8.5;
    }

    public double getCarLoanROI() {
        return 10.5;
    }

    public L_3_35_Overriding_RBIbank getObject(){
        L_3_35_Overriding_RBIbank obj = new L_3_35_Overriding_RBIbank();
        return obj;

    }

    //WebDriver driver = new FirefoxDriver(); //polymorphism reference

    /*public Integer show() { //converting int to Integer class object
        return 10;
    }*/

    public Number show() {
        return 10;
    } //https://docs.oracle.com/javase/7/docs/api/java/lang/Number.htmlv
}
