public class L_3_11_Home2 {

    public static void main(String[] args) {

        L_3_11_Home h = new L_3_11_Home(); // h--> Object i = 123
        h.i++; //Object1 --> i = 124

        L_3_11_Home h1 = new L_3_11_Home(); // h1 --> Object i = 123
        h1.i++; // Object2 --> i = 124

        L_3_11_Home h2 = new L_3_11_Home(); // h2 --> Object i = 123
        System.out.println(h2.i); // i = 123
    }
}
