public class L_3_18_MethodCalling {
    public static void main(String[] args) {

        //Static Component --> can't give a call non-static component directly

        //Static --> can call any static component
        //non-static --> can call non-static AND static with creating an Object

        //static-->00

        /*
        *
        * Compile
        * Executed-->JVM
         */
        L_3_18_MethodCalling m = new L_3_18_MethodCalling();
        m.go();

        go3(); //static method call
    }


        public static void  go3(){
            System.out.println("Inside Go3 Method"); // static method
        }

        public void go() {
            System.out.println("Inside Go Method");
            go1();
        }

        public void go1() {
            System.out.println("Inside Go1 Method");
            go2();
        }

        public void go2() {
            System.out.println("Inside Go2 Method");
        }
}

