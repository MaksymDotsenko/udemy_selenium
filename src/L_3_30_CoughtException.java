public class L_3_30_CoughtException {
    public static void main(String[] args) throws InterruptedException{
    //public static void main(String[] args) { //try and catch variation

        int i[] = new int[4];

        i[5] = 100;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException t) {

            t.printStackTrace();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException t) {

                t.printStackTrace();
    }
        Thread.sleep(1000); // gives an error --> declaration "throws" in psvm
    }
}
