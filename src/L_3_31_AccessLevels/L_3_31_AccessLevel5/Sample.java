package L_3_31_AccessLevels.L_3_31_AccessLevel5;

import L_3_31_AccessLevels.L_3_31_AccessLevel4.Test;

public class Sample {
    public static void main(String[] args) {


        /*
        *Inheritance (Nasledovanie) - one class object will inherit other class properties
        * extends, implements
         */
        Test obj = new Test();

        System.out.println(obj.publicVariable);
        //System.out.println(obj.privateVariable); //--> only in class
        //System.out.println(obj.protectedVariable); //--> only in same package
        //System.out.println(obj.defauldVariable); //--> only in same package

        /*Sample s =new Sample();
        System.out.println(s.publicVariable);*/
    }
}
