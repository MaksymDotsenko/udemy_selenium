package L_3_31_AccessLevels.L_3_31_AccessLevel5;

import L_3_31_AccessLevels.L_3_31_AccessLevel4.Test;

public class Inheritance extends Test {

    public static void main(String[] args) {


        /*
         *Inheritance (Nasledovanie) - one class object will inherit other class properties
         * extends, implements
         *
         * pakg5/Inhritance extends all property of pakg4/Test class
         */
        Inheritance obj = new Inheritance();

        System.out.println(obj.publicVariable);
        //System.out.println(obj.privateVariable); //--> only in class
        System.out.println(obj.protectedVariable); //--> only in same package/exept extension
        //System.out.println(obj.defauldVariable); //--> only in same package

        /*Sample s =new Sample();
        System.out.println(s.publicVariable);*/
    }
}
