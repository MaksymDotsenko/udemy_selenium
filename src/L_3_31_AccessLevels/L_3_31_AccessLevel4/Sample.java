package L_3_31_AccessLevels.L_3_31_AccessLevel4;

public class Sample {

    public static void main(String[] args) {

        Test obj = new Test();

        System.out.println(obj.publicVariable);
        //System.out.println(obj.privateVariable); //--> only in class
        System.out.println(obj.protectedVariable);
        System.out.println(obj.defauldVariable);
    }
}
