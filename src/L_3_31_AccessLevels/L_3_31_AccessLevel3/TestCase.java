package L_3_31_AccessLevels.L_3_31_AccessLevel3;

import L_3_31_AccessLevels.L_3_31_AccessLevel2.ClassA;



// import L_3_31_AccessLevels.L_3_31_AccessLevel2.ClassB; --> default access level


    public class TestCase {
        public static void main(String[] args) {

            ClassA obj = new ClassA();
            obj.add();

            /*ClassB obj2 = new ClassB();
            obj2.show();*/
        }

}
