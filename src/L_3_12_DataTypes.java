public class L_3_12_DataTypes {

    public static void main(String[] args) {

        // primitives datatypes

        int i = 1234567890;
        float f = 1.23423434442432342f;
        boolean b = true; // or false
        long l = 12345678901l;
        double d = 1.23234234234234234;
        char c = 'a'; // stors a single character
        String s = "This is Java"; //Class in Java

        //non-primitive datatype

        L_3_12_DataTypes dot = new L_3_12_DataTypes(); //only objects that made by programers
        String abc = new String(); // String is a class in Java
        abc = "Hello";

        String s1 = "Way"; //spaces "Way  "
        String s2 = "2";
        String s3 = "Automation";
        String s4 = s1+s2+s3; //spaces -> s1+""s2""+s3;

        //String + String -> Concatenated String
        System.out.println(s4); // Way2Automation
        System.out.println(s1+s2+s3);
        System.out.println("Way"+"2"+"Automation");

        //int + int -->int
        int num1 = 10;
        int num2 = 5;
        System.out.println(num1+num2); //result 15
        System.out.println(num1+num2+"result"); //15result
        System.out.println("string"+num1+num2+"result"); //string105result !!! Cous of copmilation strats from left to right
        System.out.println("string"+(num1+num2)+"result"); //string15result !!! start reading from breceets


        /*
        * 12+45 -> Operands
        *
        *
        * Unary Operators --> single operand
        * Binary Operators --> two operand
        * Ternary Operators --> 3 operand000
        *
         */

        System.out.println(34==74); // false
        System.out.println(34==34); // true
        System.out.println(34>=74); // false
        System.out.println(34<=74); // true
        //System.out.println(34 && 74); // not working
        System.out.println(true && true); // true
        System.out.println(true && false); // false
        System.out.println((123==123) && (34==34)); // true
        System.out.println((123==354) && (34==34)); // false
        System.out.println((123==123) || (34==34)); // true OR operator
        System.out.println((123==123) || (34==34)); // true OR operator
    }
}
