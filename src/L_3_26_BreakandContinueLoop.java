public class L_3_26_BreakandContinueLoop {

    public void go(){
        System.out.println("First");

        //break; --> can't be used outside of a loop or a switch
        //continue; --> can't be used outside of a loop

        System.out.println("Last");
    }
    public static void main(String[] args) {

        /*L_3_26_BreakandContinueLoop b= new L_3_26_BreakandContinueLoop();
        b.go();*/

        for(int i=0; i<10; i++){

            if(i>=5 && i<=7)
                continue; // continue --> continue to the next cycle --> used to skip iteration
            System.out.println(i);

            /*if(i==5)
                break;
            System.out.println(i);*/
        }
        System.out.println("Outside the loop");
    }
}
