public class L_3_24_ForLoop {
    public static void main(String[] args) {

        /*
        *
        * while--> when the number of interations are unknown
        * For--> number of interation are known
        * do While--< at least for one time irrespective of the condition
         */
        for(int i=1; i<=10; i++){
            System.out.println(i);


        }
    }
}
