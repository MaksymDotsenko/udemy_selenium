public class L_3_17_CalculatorLessons {

    public int getSum (int num1, int num2){ //parameters
        int result = num1+num2;
        return result;
    }

    public int getSub (int num1, int num2){

        return num1-num2;
    }

    public int getMul (int num1, int num2){

        return num1*num2;
    }

    public int getDiv (int num1, int num2){

        return num1/num2;
    }

    public static void main(String[] args) {
        L_3_17_CalculatorLessons calc = new L_3_17_CalculatorLessons();
        int i = calc.getSum(10, 4);
        System.out.println("The Sum is: "+i);

        System.out.println("The Mult is: " + calc.getMul(11, 5));
        System.out.println("The Sub is: " + calc.getSub(10, 2));
        System.out.println("The Div is: " + calc.getDiv(500, 10));
    }
}
