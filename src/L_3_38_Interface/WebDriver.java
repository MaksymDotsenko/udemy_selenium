package L_3_38_Interface;

public interface WebDriver { //if use "public final" interface than we can not override methods

    public void click();
    public void sendKeys();
}
