package L_3_38_Interface;

public interface RemoteWebDriver extends WebDriver {

    public void windowMaximize();
}
