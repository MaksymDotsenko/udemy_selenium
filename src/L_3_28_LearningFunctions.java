public class L_3_28_LearningFunctions {

    public static int a = 30;
    public static int b = 20;

    public static void print(){

        System.out.println("Learning Functions");
    }

    public static void addNumbers(){

        int c = a+b;
        System.out.println("Adding of two numbers are: "+c);
    }

    public static void addNumbers(int r, int s){
        int add = r+s;
        System.out.println("Adding of two numbers are: "+add);

    }

    //OR

    /*public static int addNumbers(int r, int s){
        int add = r+s;
        return add;
    }*/

    public static void main(String[] args) {

        print();
        addNumbers();
        addNumbers(30,40);
        //int res = addNumbers(30,40);
        //System.out.println(res);
    }
}
