public class L_3_29_Exceptions {
    public static void main(String[] args) {
        System.out.println("Before try block");

        try {
            System.out.println("Begining");

            int devide =10/0;
            //int devide = 10/0; --> zero exception

            System.out.println(devide);
            System.out.println("Ending");

        }catch (Throwable t){
            System.out.println("Error occurred");
            System.out.println(t.getMessage()); //exception error message
            t.printStackTrace(); //full exception error message + line
        }
        System.out.println("After try catch block");
    }
}
