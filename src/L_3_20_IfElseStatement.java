import java.sql.SQLOutput;

public class L_3_20_IfElseStatement {
    public static void main(String[] args) {

        int num = (int)(Math.random()*20);
        System.out.println(num);

        //if (true) { // condition-->true

        if (num>=10)
            System.out.println(num+" Number is greater than 10");
        else if(num<10 && num<5)
            System.out.println(num+" Number is between 10 and 5");
        else
            System.out.println(num+" Number less than 10");


    }
}
