package L_3_37_Abstraction;

public class Test {

    public static void main(String[] args) {

        //new WebDriver(); //can't becouse it is abstract

        ChildFirefox c = new ChildFirefox();
        c.captureScreenshot();
        c.childMethod();

        WebDriver d = new ChildFirefox();
        d.captureScreenshot();
        d.click();
        d.getTitel();
        //d.childMethod(); //can't be done but...
        ((ChildFirefox) d).childMethod();
    }
}
