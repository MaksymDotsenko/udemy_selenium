package L_3_37_Abstraction;

public abstract class WebDriver {

    public abstract void click();

    public abstract void sendKeys();

    public abstract void getTitel();

    public void captureScreenshot(){

    }
}
