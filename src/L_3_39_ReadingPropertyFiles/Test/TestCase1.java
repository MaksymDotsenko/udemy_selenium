package L_3_39_ReadingPropertyFiles.Test;

import java.io.FileInputStream;
import java.util.Properties;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestCase1 {

    public static void main(String[] args) throws IOException{


        Properties prop = new Properties();
        //FileInputStream fis = new FileInputStream("/home/maksymd/IdeaProjects/Java_Lecture2/src/L_3_39_ReadingPropertyFiles/Config/object.properties");

        // OR
        FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"/src/L_3_39_ReadingPropertyFiles/Config/object.properties"); //in this way better to use to keep project location as dynamic!
        prop.load(fis);

        System.out.println("Name: "+prop.getProperty("name"));
        System.out.println("Course: "+prop.getProperty("course"));
        System.out.println("Age: "+prop.getProperty("age"));
        System.out.println("Salary: "+prop.getProperty("salary"));

        System.out.println(System.getProperty("user.dir"));
    }
}
