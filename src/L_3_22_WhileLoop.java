
public class L_3_22_WhileLoop {

    /*
    *
    * Loops are not statement--> they are not terminated but exist
    * Entry criteria --> loop condition must be True!
    * Exit criteria --> loop condition is False!
    *
     */

    public static void main(String[] args) {


       /* while(true){

        }*/

        /*while(10>8){

        }*/
        int i = 1;
        while(/*int*/ i<=10){//int can't be in gate keeper
            System.out.println(i);
            i++;
        }

    }

}
