public class L_3_17_Calculator {
    /*
     * ClassName - TheDarkNight
     * methodName - theDarkNight
     *
     * //CTRL+SHIFT+F - aling (выравнивать)
     * */
    public L_3_17_Calculator(){

        System.out.println("Calling constructor");
    }
    // other
    public void add() {
        System.out.println("Adding some numbers");

    }

    public void sub() {
        System.out.println("Sub some numbers");

    }

    public void div() {
        System.out.println("Div some numbers");

    }

    public void mult() {
        System.out.println("Mult some numbers");

    }

    public static void main(String[] args){

        L_3_17_Calculator calc = new L_3_17_Calculator();
        calc.add();
        calc.div();
        calc.mult();
        calc.sub();

        new L_3_17_Calculator().sub();
        new L_3_17_Calculator().mult();
        new L_3_17_Calculator().div();
        new L_3_17_Calculator().add();


        System.out.println("Hello World !!!");
    }
}
