package L_3_32_Encapsulation;

public class Bank {

    private int accountNo=125236;
    private int pinNo=1234;
    private double balanceAmount=100000;

    public void withDrawAmount(int accNo, int pin, int amount){

        if(accNo==accountNo && pin==pinNo){

            if(amount<=balanceAmount){
                balanceAmount=balanceAmount-amount;
                System.out.println("Amount withdraw : "+amount);
            }else{
                System.out.println("Insufficient Balance!!!");
            }
        } else{
            System.out.println("Invalid credentials!!!");
        }
    }

    public void updatePin(int accNo, int oldPin, int newPin){
        if(accNo==accountNo && oldPin==pinNo){

            pinNo=newPin;
            System.out.println("Pin changed successfully");

        }else{
            System.out.println("Invalid credentials!!!");
        }
    }

    public double depositCash(int accNo, int pin, double amount){

        if(accNo==accountNo && pin==pinNo){
            balanceAmount=balanceAmount+amount;
            return balanceAmount;
        }else{
            System.out.println("Invalid Credentials!!!");
            return balanceAmount;
        }
    }

    public int getAccountNo() {
        return accountNo;
    }

    public int getPinNo() {
        return pinNo;
    }

    public void setPinNo(int pinNo) {
        this.pinNo = pinNo;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
}
