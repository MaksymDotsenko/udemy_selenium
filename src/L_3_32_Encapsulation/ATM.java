package L_3_32_Encapsulation;

public class ATM {

    public static void main(String[] args) {


        Bank obj =new Bank();
        //obj.pinNo=2222; //--> private, can;t be updated
        obj.updatePin(125236, 1234, 3333);

        //obj.accountNo=98978;
        obj.withDrawAmount(125236, 3333, 100);
    }
}
